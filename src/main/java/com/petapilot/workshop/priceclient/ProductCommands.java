package com.petapilot.workshop.priceclient;

import com.petapilot.workshop.client.api.ProductApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class ProductCommands {

    private final ProductApi productApi;

    @Autowired
    public ProductCommands(ProductApi productApi) {
        this.productApi = productApi;
    }

    @ShellMethod("Returns the AVG price of a Product")
    public String productAvgPrice(@ShellOption Integer productId) {
        return productApi.getProductAvgPriceUsingGET(productId).toString();
    }
}
