package com.petapilot.workshop.priceclient;

import com.petapilot.workshop.client.api.PriceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class PriceCommands {

    private final PriceApi priceApi;

    @Autowired
    public PriceCommands(PriceApi priceApi) {
        this.priceApi = priceApi;
    }

    @ShellMethod("Returns the price by ID")
    public String price(@ShellOption Integer priceId) {
        return priceApi.getPriceUsingGET(priceId).toString();
    }
}
